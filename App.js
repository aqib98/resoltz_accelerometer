import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import ScrollableTabView, {DefaultTabBar, } from 'react-native-scrollable-tab-view';

import SensorExample from './SensorExample';
import SensorExample1 from './SensorExample1'
import DecoratorExample from './DecoratorExample';

export default class App extends Component {
  render() {
    return (
      <ScrollableTabView
        style={{marginTop: 20, }}
        renderTabBar={() => <DefaultTabBar />}
      >
        <SensorExample1 tabLabel = "SensorExample"/>
        <DecoratorExample tabLabel="SensorAwareView" />
      </ScrollableTabView>
    );
  }
}
