var notificationController = require('./notification.controller');
const router = require('express').Router();
router.post("/",notificationController.sendnotification);
router.get("/:student_id",notificationController.getnotification);
router.post("/deviceregistration",notificationController.FCMSave);
router.post("/send",notificationController.FCMSend);
module.exports = router;
