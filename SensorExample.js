import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,

} from 'react-native';

import RNSensors from 'react-native-sensors';
const { Accelerometer, Gyroscope } = RNSensors;
const accelerationObservable = new Accelerometer({
  updateInterval: 100, // defaults to 100ms
});

const gyroscopeObservable = new Gyroscope({
  updateInterval: 2000, // defaults to 100ms
});

export default class SensorExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      acceleration: {
        x: 'unknown',
        y: 'unknown',
        z: 'unknown',
      },
      gyroscope: {
        x: 'unknown',
        y: 'unknown',
        z: 'unknown',
      },
      length:0,
      length1:0,
      stepCountFlag:false,
      stepCount:0,
      gravity : {
        x: 0,
        y: 0,
        z: 0,
      },
      FilteredAccleration : {
        x: 0,
        y: 0,
        z: 0,
      },
      emaFilter : {
        x : 0,
        y : 0,
        z : 0,
      },
      sumAverage : 0,
      alpha : 0.8,
      alpha1 : 0.8,
      threshold : 0.7,
      loopCount : 0,
      minmaxFlag : false,
      frequencyCount : 0,
    };
  }

  componentWillMount() {

    accelerationObservable
      .subscribe(acceleration => this.setState({
        acceleration,
      },function(){

      this.state.gravity.x = this.state.alpha*this.state.gravity.x + (1-this.state.alpha)*this.state.acceleration.x
      this.state.gravity.y = this.state.alpha*this.state.gravity.y + (1-this.state.alpha)*this.state.acceleration.y
      this.state.gravity.z = this.state.alpha*this.state.gravity.z + (1-this.state.alpha)*this.state.acceleration.z
      let tempObj = {}
      tempObj.x = this.state.acceleration.x-this.state.gravity.x;
      tempObj.y = this.state.acceleration.y-this.state.gravity.y;
      tempObj.z = this.state.acceleration.z-this.state.gravity.z;
      this.setState({FilteredAccleration:tempObj},function(){
          let tempObj1 = {}
          tempObj1.x = (1-this.state.alpha1)*this.state.emaFilter.x + this.state.alpha1*this.state.FilteredAccleration.x;
          tempObj1.y = (1-this.state.alpha1)*this.state.emaFilter.y + this.state.alpha1*this.state.FilteredAccleration.y;
          tempObj1.z = (1-this.state.alpha1)*this.state.emaFilter.z + this.state.alpha1*this.state.FilteredAccleration.z;
          this.setState({emaFilter:tempObj1})
      })

      let x = this.state.FilteredAccleration.x;
      let y = this.state.FilteredAccleration.y;
      let z = this.state.FilteredAccleration.z;

      let length = Math.sqrt(x*x + y*y + z*z)
      let length1 = Math.sqrt(this.state.emaFilter.x*this.state.emaFilter.x+this.state.emaFilter.y*this.state.emaFilter.y+this.state.emaFilter.z*this.state.emaFilter.z)
      this.setState({length1:length1},function(){
        if(this.state.frequencyCount<1000){
          this.setState({frequencyCount:this.state.frequencyCount+1})

        }
        if(this.state.loopCount<50){
          this.setState({sumAverage:this.state.sumAverage+this.state.length1,loopCount:this.state.loopCount+1});

        }
        else{
          this.setState({threshold:this.state.sumAverage/50,loopCount:0,sumAverage:0})
        }

        if(this.state.stepCountFlag===false){
          //console.log('try',this.state.length1,this.state.threshold)
          if(this.state.length1>this.state.threshold){


            this.setState({stepCountFlag:true},function(){
            //console.log('if',this.state.stepCountFlag,this.state.length,this.state.threshold)
            })
          }

        }
        else if(this.state.stepCountFlag===true){
          if(this.state.length1<this.state.threshold){
            if(this.state.frequencyCount<1000){
              if(this.state.threshold>0.6){
                this.setState({stepCountFlag:false,stepCount:this.state.stepCount+1},function(){
                  //console.log('elseif',this.state.stepCountFlag,this.state.length,this.state.threshold)

                })
              }
            }
            else{
              this.setState({frequencyCount:1001})
              if(this.state.threshold>0.2){

                // if(this.state.frequencyCount>50 && this.state.frequencyCount<200){
                  this.setState({stepCountFlag:false,stepCount:this.state.stepCount+1,frequencyCount:0},function(){
                    //console.log('elseif',this.state.stepCountFlag,this.state.length,this.state.threshold)

                  })
                // }
                // else{
                //   //console.log('else'+this.state.frequencyCount)
                // }

              }
            }



          }
        }
      })
      this.setState({length:length},function(){


      })
    }));

    gyroscopeObservable
      .subscribe(gyroscope => this.setState({
        gyroscope,
      }));

  }

  componentDidMount(){

  }

  onPressLearnMore = (e)=> {
    e.preventDefault()
    this.setState({stepCount:0,frequencyCount:0})
  }

  render() {
    const {
      acceleration,
      gyroscope,
      length,
      length1,
      stepCount,
      FilteredAccleration,
      gravity,
      emaFilter
    } = this.state;

    return (
      <View style={styles.container}>

        <Text style={styles.welcome}>
          Length:
        </Text>
        <Text style={styles.instructions}>
          {length + '/' + length1}
        </Text>
        <Text style={styles.welcome}>
          stepCount:
        </Text>
        <Text style={styles.instructions}>
          {stepCount}
        </Text>
        <Button
        onPress={this.onPressLearnMore}
        title="Reset"
        color="#841584"
        accessibilityLabel="Learn more about this purple button"
        />
      </View>
    );
  }

  componentWillUnmount() {
    accelerationObservable.stop();
    gyroscopeObservable.stop();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
