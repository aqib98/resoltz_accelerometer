import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  DeviceEventEmitter,
  NativeModules,
  Image
} from 'react-native';

let mSensorManager = NativeModules.SensorManager;

import RNSensors from 'react-native-sensors';





export default class SensorExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stepCountData:'unknown'
    };
  }

  componentWillMount(){
    SensorManager.startStepCounter(1000);
    DeviceEventEmitter.addListener('StepCounter', function (data) {
        console.log(data)
        this.setState({stepCountData:data})
    });
  SensorManager.stopStepCounter();
  }

  render() {
    return (
      <View>
        <Text>Step Count</Text>
        <Text>{this.state.stepCountData}</Text>
      </View>
    );
  }


}

const styles = StyleSheet.create({

});
